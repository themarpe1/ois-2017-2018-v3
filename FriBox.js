if (!process.env.PORT) {
  process.env.PORT = 8080;
}

var mime = require('mime-types');
var formidable = require('formidable');
var http = require('http');
var fs = require('fs-extra');
var util = require('util');
var path = require('path');

var dataDir = "./data/";

var streznik = http.createServer(function(zahteva, odgovor) {
  if (zahteva.url == '/') {
    posredujOsnovnoStran(odgovor);
  } else if (zahteva.url == '/datoteke') {
    posredujSeznamDatotek(odgovor);
  } else if (zahteva.url.startsWith('/brisi')) {
    izbrisiDatoteko(odgovor, dataDir + zahteva.url.replace("/brisi/", ""));
  } else if (zahteva.url.startsWith('/prenesi')) {
    posredujStaticnoVsebino(odgovor, dataDir + zahteva.url.replace("/prenesi", ""), "application/octet-stream");
  } else if (zahteva.url == "/nalozi") {
    naloziDatoteko(zahteva, odgovor);
  } else if (zahteva.url.startsWith("/poglej")){
    posredujStaticnoVsebino(odgovor, dataDir + zahteva.url.replace("/poglej", ""));
  } else {
    posredujStaticnoVsebino(odgovor, './public' + zahteva.url, "");
  }
});

streznik.listen(process.env.PORT);
console.log("Started");

function posredujOsnovnoStran(odgovor) {
  posredujStaticnoVsebino(odgovor, './public/fribox.html', "");
}

function posredujStaticnoVsebino(odgovor, absolutnaPotDoDatoteke, mimeType) {
  fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
    if (datotekaObstaja) {
      fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
        if (napaka) {
          //Posreduj napako, rip on serverside
          error500(odgovor);
          
        } else {
          posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina, mimeType);
        }
      });
    } else {
      //Posreduj napako
      error404(odgovor);
    }
  });
}

function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina, mimeType) {
  if (mimeType == "") {
    odgovor.writeHead(200, {'Content-Type': mime.lookup(path.basename(datotekaPot))});
  } else {
    odgovor.writeHead(200, {'Content-Type': mimeType});
  }

  odgovor.end(datotekaVsebina);
}

function posredujSeznamDatotek(odgovor) {
  odgovor.writeHead(200, {'Content-Type': 'application/json'});
  fs.readdir(dataDir, function(napaka, datoteke) {
    if (napaka) {
      //Posreduj napako
      error500(odgovor);
    } else {
      var rezultat = [];
      for (var i=0; i<datoteke.length; i++) {
        var datoteka = datoteke[i];
        var velikost = fs.statSync(dataDir+datoteka).size;
        rezultat.push({datoteka: datoteka, velikost: velikost});
      }

      odgovor.write(JSON.stringify(rezultat));
      odgovor.end();
    }
  });
}

function naloziDatoteko(zahteva, odgovor) {
  var form = new formidable.IncomingForm();

  form.parse(zahteva, function(napaka, polja, datoteke) {
    util.inspect({fields: polja, files: datoteke});
  });

  form.on('end', function(fields, files) {
    var zacasnaPot = this.openedFiles[0].path;
    var datoteka = this.openedFiles[0].name;
    
    //check if same file already exists on the server
    if(!fs.existsSync(dataDir + datoteka)){
      //file does not exist... proceed with upload
      fs.copy(zacasnaPot, dataDir + datoteka, function(napaka) {
        if (napaka) {
          //Posreduj napako
          error500(odgovor);
        } else {
          posredujOsnovnoStran(odgovor);
        }
      });  
    } else {
      error409(odgovor); 
    }
    
    
  });
}

function izbrisiDatoteko(odgovor, pot){
  console.log("About to delete: " + pot);
  fs.unlink(pot, (err) => {
    console.log(pot + ", should be deleted");
    
    if (err){
      odgovor.writeHead(200, {"Content-Type": "text/plain"});
      odgovor.end("Datoteka ni bila izbrisana");
    } else {
      odgovor.writeHead(200, {"Content-Type": "text/plain"});
      odgovor.end("Datoteka izbrisana");
    }
    
  });
}

function error404(odgovor){
  odgovor.writeHead(404, {'Content-Type': 'text/html'});
  odgovor.write("<h1>Woops, this does not exist...</h1>");
  odgovor.end();
}

function error409(odgovor){
  odgovor.writeHead(409, {'Content-Type': 'text/html'});
  odgovor.write("<h1>Unfortunately the file already exists on the server.</h1>");
  odgovor.end();
}

function error500(odgovor){
  odgovor.writeHead(500, {'Content-Type': 'text/html'});
  odgovor.write("<h1>Server had an error processing your request, please try again later...</h1>");
  odgovor.end();
}
